<?php

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['AUTH_FORM_MESSAGE'] = "Вам необходимо авторизоваться";
$MESS['SETTINGS_INPUT_PONE_FORMAT'] = "Форматировать в лидах поле телефон ";
$MESS['SETTINGS_OPTIONS_RESTORED'] = "Восстановлены настройки по умолчанию";
$MESS['SETTINGS_OPTIONS_SAVED'] = "Настройки сохранены";

