<?php

namespace DC\LeadPhone;

use DC\LeadPhone\Formatters\PhoneFormatter;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Event
{
    
    /**
     * Обработка полей лида
     * @param $arFields
     * @return void
     */
    public static function eventProcess(&$arFields): void
    {
        $value = Option::get('dc.leadphone', "format_phone", "N");
        if ('Y' === $value) {
            if ($arFields['FM']['PHONE']) {
                foreach ($arFields['FM']['PHONE'] as &$phoneData) {
                    if ($phone = $phoneData['VALUE']) {
                        $phoneData['VALUE'] = PhoneFormatter::format($phone);
                    }
                }
                unset($phoneData);
            }
        }
    }
}
