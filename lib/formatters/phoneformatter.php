<?php

namespace DC\LeadPhone\Formatters;

class PhoneFormatter
{
    /**
     * В строке необходимо убрать все не числовые символы кроме "+" в начале строки (если он есть).
     * @param string $phone
     * @return string
     */
    public static function format(string $phone): string
    {
        $plus = '';
        if (preg_match("!^\D*\+!", $phone)) {
            $plus = '+';
        }
        $phoneClear = ($plus ?: "").preg_replace('!\D+!', '', $phone);
        return $phoneClear;
    }
}