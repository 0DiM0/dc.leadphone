<?php

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;


defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'dc.leadphone');

if (!$USER->isAdmin()) {
    $APPLICATION->AuthForm(Loc::getMessage("AUTH_FORM_MESSAGE"));
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

Loc::loadMessages($context->getServer()->getDocumentRoot()."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

$tabControl = new CAdminTabControl("tabControl", [
    [
        "DIV" => "edit1",
        "TAB" => Loc::getMessage("MAIN_TAB_SET"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_SET"),
    ],
]);


if ((!empty($save) || !empty($restore)) && $request->isPost() && check_bitrix_sessid()) {
    $requestValue = $request->getPost('format_phone') ? 'Y' : 'N';
    if (!empty($restore)) {
        Option::delete(ADMIN_MODULE_NAME);
        CAdminMessage::showMessage([
            "MESSAGE" => Loc::getMessage("SETTINGS_OPTIONS_RESTORED"),
            "TYPE" => "OK",
        ]);
    } elseif ($requestValue) {
        Option::set(ADMIN_MODULE_NAME, "format_phone", $requestValue);
        CAdminMessage::showMessage(["MESSAGE" => Loc::getMessage("SETTINGS_OPTIONS_SAVED"), "TYPE" => "OK",]);
    }
}

$tabControl->begin();
?>

<form method="post"
      action="<?= sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID) ?>">
    <?php
    echo bitrix_sessid_post();
    $tabControl->beginNextTab();
    ?>
    <tr>
        <td width="40%">
            <label for="format_phone"><?= Loc::getMessage("SETTINGS_INPUT_PONE_FORMAT") ?>:</label>
        <td width="60%">
            <input type="checkbox"
                   size="50"
                   name="format_phone"
                   value="Y"
                <?= 'Y' == Option::get(ADMIN_MODULE_NAME, "format_phone", 'N') ? 'checked' : '' ?>/>
        </td>
    </tr>
    
    <?php
    $tabControl->buttons();
    ?>
    <input type="submit"
           name="save"
           value="<?= Loc::getMessage("MAIN_SAVE") ?>"
           title="<?= Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"
    />
    <input type="submit"
           name="restore"
           title="<?= Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           onclick="return confirm('<?= AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<?= Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>"
    />
    <?php
    $tabControl->end();
    ?>
</form>
