<?php

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use DC\LeadPhone\Event;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;


Loc::loadMessages(__FILE__);

class dc_leadphone extends CModule
{
    public function __construct()
    {
        $arModuleVersion = [];
        
        include __DIR__.'/version.php';
        
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        
        $this->MODULE_ID = 'dc.leadphone';
        $this->PARTNER_URI = 'http://dc.dc';
        $this->MODULE_NAME = Loc::getMessage('DC_LEADPHONE_MODULE_NAME');
        $this->PARTNER_NAME = Loc::getMessage('DC_LEADPHONE_MODULE_PARTNER_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DC_LEADPHONE_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
    }
    
    
    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->registerEventHandlers();
    }
    
    public function doUninstall()
    {
        $this->unRegisterEventHandlers();
        Option::delete($this->MODULE_ID);
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
    
    /**
     * Добавляет обработчики событий
     * @return void
     */
    public function registerEventHandlers()
    {
        $eventManager = EventManager::getInstance();
        foreach (self::getEventParams() as $arrParams) {
            $eventManager->registerEventHandler(
                "crm",
                $arrParams['eventType'],
                $this->MODULE_ID,
                $arrParams['class'],
                $arrParams['method']
            );
        }
    }
    
    /**
     * Удаляет обработчики событий
     * @return void
     */
    public function unRegisterEventHandlers()
    {
        $eventManager = EventManager::getInstance();
        foreach (self::getEventParams() as $arrParams) {
            $eventManager->unRegisterEventHandler(
                "crm",
                $arrParams['eventType'],
                $this->MODULE_ID,
                $arrParams['class'],
                $arrParams['method']
            );
        }
    }
    
    
    /**
     * Получаем параметры для событий к которым применяем форматтер
     * @return array[]
     */
    private static function getEventParams(): array
    {
        $arrParams = [
            [
                'eventType' => 'OnBeforeCrmLeadAdd',
                'class' => Event::class,
                'method' => 'eventProcess',
            ],
            [
                'eventType' => 'OnBeforeCrmLeadUpdate',
                'class' => Event::class,
                'method' => 'eventProcess',
            ],
        ];
        return $arrParams;
    }
}
